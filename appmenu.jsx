import React from 'react';
import { useSelector } from 'react-redux';
import { Scrollbars } from 'react-custom-scrollbars';

function AppMenuItem({key, appid, icon, name, onClick}) {
    const [ hovered, setHovered ] = React.useState(false);
    return (
        <div key={appid} style={{
            background: (hovered)?'rgba(29, 32, 33, 0.8)':'transparent',
            transition: 'background .3s',
            padding:'0.4rem',
            width:'5rem',
            display:'flex',
            flexDirection:'column',
            height:'min-content'
        }} onClick={onClick} onTouchStart={()=>setHovered(true)} onTouchEnd={()=>setHovered(false)}>
          <img alt="" src={icon} style={{
              width:'calc(100% - 0.4rem)',
              filter:'brightness(110%)',
              padding:'0.2rem'
          }}/>
          <div style={{
              color:'white',
              height:'100%',
              display:'flex',
              alignItems:'center',
              justifyContent:'center'
          }}>
            {name}
          </div>
        </div>
    );
}

function MobileAppMenu({shown, onClose=()=>{}}) {

    const am = useSelector(state => state.am);


    let apps = [];
    for(let [appid, app] of Object.entries(am.am.listApps())) {
        if(appid!==app.id) continue;
        
        apps.push(<AppMenuItem key={app.id} appid={app.id} icon={app.icon} name={app.name} onClick={()=>{
            onClose();
            if(Object.keys(am.windows).indexOf(appid)!==-1) {
                am.am.getAppById(appid).dispatch(new Event('focus'));
                if(am.hidden[appid]) am.am.toggleHideApp(appid);
            } else {
                am.am.startApp('mobile',app.id);
            }
        }}/>);
    }

    return (
        <div style={{
            height:'calc(100%)',
            top:(shown)?0:'101%',
            transition:'top .4s',
            left:0,
            width:'calc(100%)',
            background:'rgba(0,0,0,0.4)',
            position:'absolute',
        }}>
          <Scrollbars>
            <div style={{
                height:'calc(100% - 0.8rem)',
                width:'calc(100% - 0.8rem)',
                padding:'0.4rem',
                display:'flex',
                flexDirection:'row',
                flexWrap:'wrap'
            }}>
              {apps}
            </div>
        </Scrollbars>
        </div>
    );
}
export {
    MobileAppMenu
}
